package com.pokedex.api.validation.constraint;

import com.pokedex.api.validation.validator.GenderValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * GenderConstraint .
 *
 * @author Carlos
 */
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = GenderValidator.class)
@NotBlank
@NotNull
public @interface GenderConstraint {

  /**
   * message .
   * @return String
   */
  String message() default "{pokemon.gender.contains}";

  /**
   * groups .
   * @return
   */
  Class<?>[] groups() default { };

  /**
   * payload
   * @return
   */
  Class<? extends Payload>[] payload() default { };

}
