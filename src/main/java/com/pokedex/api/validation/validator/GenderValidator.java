package com.pokedex.api.validation.validator;

import com.pokedex.api.validation.constraint.GenderConstraint;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * GenderValidator .
 *
 * @author Carlos
 */
public class GenderValidator implements ConstraintValidator<GenderConstraint,String> {
  private static  final String[] GENDERS = new String[] {"male","female"};

  @Override
  public boolean isValid(String gender, ConstraintValidatorContext context) {
    return Arrays.asList(GENDERS).contains(gender);
  }
}
