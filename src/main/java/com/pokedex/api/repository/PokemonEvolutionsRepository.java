package com.pokedex.api.repository;

import com.pokedex.api.model.entity.PokemonEvolutions;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PokemonEvolutionsRepository .
 *
 * @author Carlos
 */
@Repository
public interface PokemonEvolutionsRepository extends JpaRepository<PokemonEvolutions, Long> {
  Optional<PokemonEvolutions> findByPokemonName(String pokemonName);
}
