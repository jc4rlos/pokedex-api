package com.pokedex.api.repository.impl;

import com.pokedex.api.mappers.PokemonMapper;
import com.pokedex.api.model.dto.PokemonRegionDto;
import com.pokedex.api.repository.PokemonCustomRepository;
import com.pokedex.api.util.PokemonQueries;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;


/**
 * PokemonCustomRepositoryImpl .
 *
 * @author Carlos
 */
public class PokemonCustomRepositoryImpl implements PokemonCustomRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  @SuppressWarnings("unchecked")
  public List<PokemonRegionDto> findAllPokemonRegionsByPokemonNameWithDto(final String pokemonName) {
    Query query = entityManager.createNativeQuery(PokemonQueries.POKEMON_REGIONS_QUERY, Tuple.class);
    query.setParameter("name", "%".concat(pokemonName).concat("%"));
    List<Tuple> pokemonRegions = query.getResultList();
    return pokemonRegions.stream()
            .map(PokemonMapper::tupleToPokemonRegionDto)
            .collect(Collectors.toList());
  }
}
