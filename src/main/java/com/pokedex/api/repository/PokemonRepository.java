package com.pokedex.api.repository;

import com.pokedex.api.model.entity.Pokemon;
import com.pokedex.api.model.projetions.PokemonRegionProjection;
import com.pokedex.api.util.PokemonQueries;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * PokemonRepository .
 *
 * @author Carlos
 */
@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Long>, PokemonCustomRepository {

  @Query(nativeQuery = true, value = PokemonQueries.POKEMON_REGIONS_QUERY)
  Collection<PokemonRegionProjection> findAllPokemonRegionsByPokemonName(@Param("name") String pokemonName);
}
