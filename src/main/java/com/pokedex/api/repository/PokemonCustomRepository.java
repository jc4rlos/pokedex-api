package com.pokedex.api.repository;

import com.pokedex.api.model.dto.PokemonRegionDto;

import java.util.List;

/**
 * PokemonCustomRepository .
 *
 * @author Carlos
 */
public interface PokemonCustomRepository {
  List<PokemonRegionDto> findAllPokemonRegionsByPokemonNameWithDto(String pokemonName);
}
