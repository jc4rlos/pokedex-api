package com.pokedex.api.mappers;

import com.pokedex.api.model.dto.PokemonDto;
import com.pokedex.api.model.dto.PokemonRegionDto;
import com.pokedex.api.model.entity.Pokemon;
import javax.persistence.Tuple;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;



/**
 * PokemonMapper .
 *
 * @author Carlos
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PokemonMapper {

  /**
   * pokemonDtoToPokemon .
   *
   * @param pokemonDto .
   * @return Pokemon
   */
  public static Pokemon pokemonDtoToPokemon(PokemonDto pokemonDto) {
    if (pokemonDto == null) {
      return null;
    }
    return Pokemon.builder()
            .id(pokemonDto.getId())
            .pokedexId(pokemonDto.getPokedexId())
            .order(pokemonDto.getOrder())
            .name(pokemonDto.getName())
            .type(pokemonDto.getType())
            .cp(pokemonDto.getCp())
            .hp(pokemonDto.getHp())
            .gender(pokemonDto.getGender())
            .weight(pokemonDto.getWeight())
            .height(pokemonDto.getHeight())
            .candy(pokemonDto.getCandy())
            .starDust(pokemonDto.getStarDust())
            .image(pokemonDto.getImage())
            .shiny(pokemonDto.isShiny())
            .favorite(pokemonDto.isFavorite())
            .evolution(pokemonDto.getEvolution())
            .requiredCandies(pokemonDto.getRequiredCandies())
            .build();
  }

  /**
   * pokemonToPokemonDto .
   *
   * @param pokemon .
   * @return PokemonDto
   */
  public static PokemonDto pokemonToPokemonDto(Pokemon pokemon) {
    if (pokemon == null) {
      return null;
    }
    return PokemonDto.builder()
            .id(pokemon.getId())
            .pokedexId(pokemon.getPokedexId())
            .order(pokemon.getOrder())
            .name(pokemon.getName())
            .type(pokemon.getType())
            .cp(pokemon.getCp())
            .hp(pokemon.getHp())
            .gender(pokemon.getGender())
            .weight(pokemon.getWeight())
            .height(pokemon.getHeight())
            .candy(pokemon.getCandy())
            .starDust(pokemon.getStarDust())
            .image(pokemon.getImage())
            .shiny(pokemon.isShiny())
            .favorite(pokemon.isFavorite())
            .evolution(pokemon.getEvolution())
            .requiredCandies(pokemon.getRequiredCandies())
            .build();
  }

  /**
   * tupleToPokemonRegionDto .
   *
   * @param tuple .
   * @return PokemonRegionDto
   */
  public static PokemonRegionDto tupleToPokemonRegionDto(Tuple tuple) {
    return PokemonRegionDto.builder()
            .id(Long.parseLong(tuple.get("id").toString()))
            .pokedexId(Long.parseLong(tuple.get("pokedex_id").toString()))
            .name(tuple.get("name", String.class))
            .type(tuple.get("type", String.class))
            .region(tuple.get("region", String.class))
            .generation(tuple.get("generation", String.class))
            .build();
  }
}
