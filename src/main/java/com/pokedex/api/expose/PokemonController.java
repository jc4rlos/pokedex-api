package com.pokedex.api.expose;

import com.pokedex.api.business.PokemonService;
import com.pokedex.api.model.dto.PokemonDto;
import com.pokedex.api.model.dto.PokemonPageDto;
import com.pokedex.api.model.dto.PokemonPatchFavorite;
import com.pokedex.api.model.dto.PokemonRegionDto;
import com.pokedex.api.model.projetions.PokemonRegionProjection;
import java.util.List;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * PokemonController .
 *
 * @author Carlos
 */
@RestController
@RequestMapping("/v1/pokemons")
public class PokemonController {

  private final PokemonService pokemonService;

  public PokemonController(PokemonService pokemonService) {
    this.pokemonService = pokemonService;
  }

  @PostMapping
  public ResponseEntity<PokemonDto> addPokemon(@RequestBody @Valid PokemonDto pokemonDto) {
    return new ResponseEntity<>(pokemonService.addPokemon(pokemonDto), HttpStatus.CREATED);
  }

  @GetMapping
  public ResponseEntity<List<PokemonDto>> findAllPokemons(@RequestParam final String orderByColumn, @RequestParam final boolean ascending) {
    return new ResponseEntity<>(pokemonService.findAllPokemons(orderByColumn, ascending), HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<PokemonDto> findByIdPokemon(@PathVariable final Long id) {
    return new ResponseEntity<>(pokemonService.findByIdPokemon(id), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<PokemonDto> deleteByIdPokemon(@PathVariable final Long id) {
    return new ResponseEntity<>(pokemonService.deleteByIdPokemon(id), HttpStatus.OK);
  }

  @PatchMapping("/make-favorite/{id}")
  public ResponseEntity<PokemonDto> patchFavoritePokemon(@Valid @RequestBody final PokemonPatchFavorite pokemonPatchFavorite,
                                                         @PathVariable final Long id) {
    return new ResponseEntity<>(pokemonService.patchFavorite(id, pokemonPatchFavorite), HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<PokemonDto> updatePokemon(@RequestBody final PokemonDto pokemonDto,
                                                  @PathVariable final Long id) {
    return new ResponseEntity<>(pokemonService.updatePokemon(id, pokemonDto), HttpStatus.OK);
  }

  @GetMapping("/pagination")
  public ResponseEntity<PokemonPageDto> findAllPagePokemons(@RequestParam(defaultValue = "0") final int page,
                                                            @RequestParam(defaultValue = "5") final int size) {
    return new ResponseEntity<>(pokemonService.findAllPagePokemon(page, size), HttpStatus.OK);
  }

  @GetMapping("/projection")
  public ResponseEntity<List<PokemonRegionProjection>> findAllPagePokemonRegionsProjectionByName(@RequestParam(defaultValue = "")
                                                                                                   final String name) {
    return new ResponseEntity<>(pokemonService.findAllPokemonRegionsByPokemonName(name), HttpStatus.OK);
  }

  @GetMapping("/dto")
  public ResponseEntity<List<PokemonRegionDto>> findAllPagePokemonRegionsProjectionByNameWithDto(@RequestParam(defaultValue = "")
                                                                                                 final String name) {
    return new ResponseEntity<>(pokemonService.findAllPokemonRegionsByPokemonNameWithDto(name), HttpStatus.OK);
  }


}
