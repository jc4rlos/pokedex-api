package com.pokedex.api.expose.advice;

import com.pokedex.api.exception.PokemonNotFoundException;
import com.pokedex.api.model.dto.ErrorResponseDto;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * PokemonControllerAdvice .
 *
 * @author Carlos
 */
@RestControllerAdvice
public class PokemonControllerAdvice {


  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(PokemonNotFoundException.class)
  public ResponseEntity<ErrorResponseDto> handlePokemonNotFoundException(PokemonNotFoundException pokemonNotFoundException) {
    ErrorResponseDto errorResponseDto = ErrorResponseDto.builder().errors(pokemonNotFoundException.getMessage()).build();
    return new ResponseEntity<>(errorResponseDto, HttpStatus.NOT_FOUND);
  }

  /**
   * handleValidationException .
   *
   * @param exception .
   * @return ResponseEntity
   */
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorResponseDto> handleValidationException(MethodArgumentNotValidException exception) {
    final List<String> errors = exception.getBindingResult().getFieldErrors().stream()
            .map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList());
    ErrorResponseDto errorResponseDto = ErrorResponseDto.builder().errors(errors).build();
    return new ResponseEntity<>(errorResponseDto, HttpStatus.BAD_REQUEST);
  }
}
