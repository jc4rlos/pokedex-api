package com.pokedex.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * SpringDataConfig .
 *
 * @author Carlos
 */
@Configuration
@EnableJpaAuditing
public class SpringDataConfig {
}
