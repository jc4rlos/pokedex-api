package com.pokedex.api.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * PokemonProperties .
 *
 * @author Carlos
 */

@Configuration
@ConfigurationProperties("api.pokemon")
@Getter
@Setter
public class PokemonProperties {

  private String orderByCp;
  private String orderByDate;
}
