package com.pokedex.api.exception;

/**
 * PokemonNotFoundException .
 *
 * @author Carlos
 */
public class PokemonNotFoundException extends RuntimeException {

  public PokemonNotFoundException(Long id) {
    super("Could not find pokemon ".concat(String.valueOf(id)));
  }

}
