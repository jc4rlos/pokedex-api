package com.pokedex.api.business.impl;

import com.pokedex.api.business.PokemonService;
import com.pokedex.api.config.PokemonProperties;
import com.pokedex.api.exception.PokemonNotFoundException;
import com.pokedex.api.mappers.PokemonMapper;
import com.pokedex.api.model.dto.PokemonDto;
import com.pokedex.api.model.dto.PokemonPageDto;
import com.pokedex.api.model.dto.PokemonPatchFavorite;
import com.pokedex.api.model.dto.PokemonRegionDto;
import com.pokedex.api.model.entity.Pokemon;
import com.pokedex.api.model.entity.PokemonEvolutions;
import com.pokedex.api.model.projetions.PokemonRegionProjection;
import com.pokedex.api.repository.PokemonEvolutionsRepository;
import com.pokedex.api.repository.PokemonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


/**
 * PokemonServiceImpl .
 *
 * @author Carlos
 */
@Service
@RequiredArgsConstructor
public class PokemonServiceImpl implements PokemonService {

  private final PokemonRepository pokemonRepository;
  private final PokemonProperties pokemonProperties;
  private final PokemonEvolutionsRepository pokemonEvolutionsRepository;

  @Override
  public PokemonDto addPokemon(PokemonDto pokemonDto) {

    Optional<PokemonEvolutions> pokemonEvolutions = pokemonEvolutionsRepository.findByPokemonName(pokemonDto.getName());

    if (pokemonEvolutions.isPresent()) {
      pokemonDto.setEvolution(pokemonEvolutions.get().getEvolution());
      pokemonDto.setRequiredCandies(pokemonEvolutions.get().getCandyAmount());
    }

    Pokemon pokemon = PokemonMapper.pokemonDtoToPokemon(pokemonDto);
    Pokemon createdPokemon = pokemonRepository.save(pokemon);
    return PokemonMapper.pokemonToPokemonDto(createdPokemon);
  }

  @Override
  public List<PokemonDto> findAllPokemons(final String orderByColumn, final boolean ascending) {
    return pokemonRepository.findAll(sortByCreatedAtAndCp(orderByColumn, ascending)).stream()
            .map(PokemonMapper::pokemonToPokemonDto)
            .collect(Collectors.toList());
  }

  @Override
  public PokemonDto findByIdPokemon(final Long id) throws PokemonNotFoundException {
    return pokemonRepository.findById(id)
            .map(PokemonMapper::pokemonToPokemonDto)
            .orElseThrow(() -> new PokemonNotFoundException(id));
  }

  @Override
  public PokemonDto deleteByIdPokemon(Long id) throws PokemonNotFoundException {
    return pokemonRepository.findById(id)
            .map(pokemon -> {
              pokemonRepository.delete(pokemon);
              return PokemonMapper.pokemonToPokemonDto(pokemon);
            })
            .orElseThrow(() -> new PokemonNotFoundException(id));
  }

  @Override
  public PokemonDto patchFavorite(final Long id, PokemonPatchFavorite pokemonPatchFavorite) throws PokemonNotFoundException {
    return pokemonRepository.findById(id)
            .map(pokemon -> {
              pokemon.setFavorite(pokemonPatchFavorite.isFavorite());
              return pokemonRepository.save(pokemon);
            })
            .map(PokemonMapper::pokemonToPokemonDto)
            .orElseThrow(() -> new PokemonNotFoundException(id));
  }

  @Override
  public PokemonDto updatePokemon(final Long id, final PokemonDto pokemonDto) throws PokemonNotFoundException {
    return pokemonRepository.findById(id)
            .map(pokemon -> PokemonMapper.pokemonDtoToPokemon(pokemonDto))
            .map(pokemonRepository::save)
            .map(PokemonMapper::pokemonToPokemonDto)
            .orElseThrow(() -> new PokemonNotFoundException(id));
  }


  private Sort sortByCreatedAtAndCp(final String orderByColumn, final boolean ascending) {

    String column = "name";

    if (orderByColumn.equals(pokemonProperties.getOrderByCp())) {
      column = "cp";
    }

    if (orderByColumn.equals(pokemonProperties.getOrderByDate())) {
      column = "createdAt";
    }

    Sort.Direction direction = ascending ? Sort.Direction.ASC : Sort.Direction.DESC;

    return Sort.by(direction, column);

  }

  @Override
  public PokemonPageDto findAllPagePokemon(int page, int size) {
    Pageable pageable = PageRequest.of(page,size);
    Page<Pokemon> pokemonPage = pokemonRepository.findAll(pageable);
    return PokemonPageDto.builder()
            .pokemons(
                    pokemonPage.getContent().stream()
                            .map(PokemonMapper::pokemonToPokemonDto)
                            .collect(Collectors.toList())
            )
            .totalItems(pokemonPage.getTotalElements())
            .totalPages(pokemonPage.getTotalPages())
            .currentPage(pokemonPage.getNumber())
            .build();
  }

  @Override
  public List<PokemonRegionProjection> findAllPokemonRegionsByPokemonName(final String pokemonName) {
    return new ArrayList<>(pokemonRepository.findAllPokemonRegionsByPokemonName("%".concat(pokemonName).concat("%")));
  }

  @Override
  public List<PokemonRegionDto> findAllPokemonRegionsByPokemonNameWithDto(String pokemonName) {
    return pokemonRepository.findAllPokemonRegionsByPokemonNameWithDto(pokemonName);
  }


}
