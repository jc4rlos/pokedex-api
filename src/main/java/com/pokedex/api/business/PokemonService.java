package com.pokedex.api.business;

import com.pokedex.api.exception.PokemonNotFoundException;
import com.pokedex.api.model.dto.PokemonDto;
import com.pokedex.api.model.dto.PokemonPageDto;
import com.pokedex.api.model.dto.PokemonPatchFavorite;
import com.pokedex.api.model.dto.PokemonRegionDto;
import com.pokedex.api.model.projetions.PokemonRegionProjection;

import java.util.List;

/**
 * PokemonService .
 *
 * @author Carlos
 */
public interface PokemonService {

  PokemonDto addPokemon(PokemonDto pokemonDto);

  List<PokemonDto> findAllPokemons(String orderByColumn, boolean ascending);

  PokemonDto findByIdPokemon(Long id) throws PokemonNotFoundException;

  PokemonDto deleteByIdPokemon(Long id) throws PokemonNotFoundException;

  PokemonDto patchFavorite(Long id, PokemonPatchFavorite pokemonPatchFavorite) throws PokemonNotFoundException;

  PokemonDto updatePokemon(Long id, PokemonDto pokemonDto) throws PokemonNotFoundException;

  PokemonPageDto findAllPagePokemon(int page, int size);

  List<PokemonRegionProjection> findAllPokemonRegionsByPokemonName(String pokemonName);

  List<PokemonRegionDto> findAllPokemonRegionsByPokemonNameWithDto(String pokemonName);
}
