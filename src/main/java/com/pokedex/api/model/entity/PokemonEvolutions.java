package com.pokedex.api.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PokemonEvolutions .
 * @author Carlos
 */
@Entity
@Table(name = "pokemon_evolutions")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PokemonEvolutions {
  @Id
  private Long id;
  @Column(name = "pokemon_name")
  private String pokemonName;
  private String evolution;
  @Column(name = "candy_amount")
  private Integer candyAmount;
}
