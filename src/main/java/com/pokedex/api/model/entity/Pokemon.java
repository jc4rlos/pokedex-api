package com.pokedex.api.model.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 * Pokemon .
 *
 * @author Carlos
 */
@Entity
@Table(name = "pokemons")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EntityListeners(AuditingEntityListener.class)
@SQLDelete(sql = "update pokemons set deleted=1 where id= ?")
@Where(clause = "deleted=0")
public class Pokemon {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "pokedex_id")
  private Long pokedexId;

  private String name;

  @Column(name = "order_number")
  private Integer order;

  private String type;
  private Integer cp;
  private Integer hp;
  private String gender;
  private BigDecimal height;
  private BigDecimal weight;
  private Integer candy;

  @Column(name = "star_dust")
  private Integer starDust;

  private String image;
  private boolean shiny;
  private boolean favorite;
  private String evolution;
  private boolean deleted;

  @Column(name = "required_candies")
  private Integer requiredCandies;

  @Column(name = "created_at")
  @CreatedDate
  private Timestamp createdAt;

  @Column(name = "updated_at")
  @LastModifiedDate
  private Timestamp updatedAt;

}
