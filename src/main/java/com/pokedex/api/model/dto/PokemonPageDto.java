package com.pokedex.api.model.dto;

import lombok.*;

import java.util.List;

/**
 * PokemonPageDto .
 *
 * @author Carlos
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokemonPageDto {
  private Long totalItems;
  private List<PokemonDto> pokemons;
  private Integer totalPages;
  private Integer currentPage;
}
