package com.pokedex.api.model.dto;

import com.pokedex.api.validation.constraint.GenderConstraint;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * PokemonDto .
 *
 * @author Carlos
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokemonDto {
  private Long id;
  private Long pokedexId;

  @NotNull(message = "El nombre no puede ser nulo")
  private String name;
  private Integer order;
  private String type;
  private Integer cp;
  private Integer hp;

  @GenderConstraint
  private String gender;
  private BigDecimal height;
  private BigDecimal weight;
  private Integer candy;
  private Integer starDust;
  private String image;
  private boolean shiny;
  private boolean favorite;
  private String evolution;
  private Integer requiredCandies;

}
