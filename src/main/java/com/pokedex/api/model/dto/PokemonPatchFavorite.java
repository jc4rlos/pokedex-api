package com.pokedex.api.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * PokemonPatchFavorite .
 *
 * @author Carlos
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokemonPatchFavorite {
  @NotNull
  private Long id;

  private boolean favorite;
}
