package com.pokedex.api.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PokemonRegionDto .
 *
 * @author Carlos
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PokemonRegionDto {
  private Long id;
  private Long pokedexId;
  private String name;
  private String type;
  private String region;
  private String generation;
}
