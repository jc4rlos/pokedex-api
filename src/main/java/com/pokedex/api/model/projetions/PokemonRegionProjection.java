package com.pokedex.api.model.projetions;

import org.springframework.beans.factory.annotation.Value;

/**
 * PokemonRegionProjection .
 *
 * @author Carlos
 */
public interface PokemonRegionProjection {
  Long getId();

  @Value("#{target.pokedex_id}")
  Long getPokedexId();

  String getName();

  String getType();

  String getRegion();

  String getGeneration();
}
